import java.util.Comparator;

public class MainApp 
{
	private ComparatorFactory factory;
	public static void main(String[] args)
	{
		MainApp theApp = new MainApp();
		theApp.start();
	}

	private void start() 
	{
		this.factory = new ComparatorFactory();
		
		if(this.factory.get("usergender") == null)
		{
			Comparator<User> comp = new UserGenderComparator();
			this.factory.add("usergender",comp);
		}
	}
	
}
