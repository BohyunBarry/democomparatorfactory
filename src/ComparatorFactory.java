import java.util.Comparator;
import java.util.HashMap;

public class ComparatorFactory 
{
	private HashMap<String, Comparator<User>> map;
	
	public ComparatorFactory()
	{
		this.map = new HashMap<String, Comparator<User>>();
	}
	public void add(String name, Comparator<User> comp)
	{
		if(!this.map.containsKey(name))
			this.map.put(name,comp);
	}
	
	public Comparator<User> get(String name)
	{
		return this.map.get(name);
	}
}
