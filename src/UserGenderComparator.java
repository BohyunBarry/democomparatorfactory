import java.util.Comparator;

public class UserGenderComparator  implements Comparator<User>
{
	
	 public UserGenderComparator()
	 {
		 
	 }

	@Override
	public int compare(User o1, User o2) 
	{
		// male, Female
		if(!o1.isGender() && o2.isGender())
		{
			return 1;
		}
		//Femal,Male
		else if(o1.isGender() && o2.isGender())
		{
			return -1;
		}
		//Same
		return 0;
	}
	
}
